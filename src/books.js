export const data = [

  {
    id: 1,
    img: "https://images-na.ssl-images-amazon.com/images/I/8144Vic9C5L._AC_UL320_SR320,320_.jpg",
    title: 'I love to the Moon and Backssss',
    author: 'Amelia Hepworth', 
  },
  
  {
    id: 2,
    img: "https://images-eu.ssl-images-amazon.com/images/I/510g8NLbpNL._SX198_BO1,204,203,200_QL40_ML2_.jpg",
    title: 'Our Class is a Family',
    author: 'Shannon Olsen' 
  },
   {
     id: 3,
    img: "https://images-eu.ssl-images-amazon.com/images/I/41EzNnr4YUL._SY264_BO1,204,203,200_QL40_ML2_.jpg",
    title: 'The Vanishing Half: A Novel',
    author: 'Brit Bennett' 
  }, 
];